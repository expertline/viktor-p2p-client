# viktor-p2p-client

## Installation

In your package.json add:
```
"viktor-p2p-client": "https://expertline@bitbucket.org/expertline/viktor-p2p-client.git"
```

## Usage

```
const username = "John";
const serverUrl = "https://your.domain.com";
const roomId = "1";

let connection = new Connection(username, serverUrl, roomId);

connection.onAddVideoStream = function addVideo(stream, user, isLocal) {};
connection.onRemoveVideoStream = function removeVideo(userId) {};
connection.onError = function (error) {};
connection.onUserSignup(user) {};
connection.onUserDisconnected(userId) {};

connection.start();
connection.stop();
```
