/* global navigator */

const SimplePeer = require('simple-peer');
const io = require('socket.io-client');
const axios = require('axios');

// encode base 64
function atob(text) {
  return new Buffer(text).toString('base64');
}

// decode base 64
function btoa(base64) {
  return new Buffer(base64, 'base64').toString();
}

class User {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

class Connection {
  constructor(name, serverUrl, roomId, passiveMode, debug, isSafariIOS = false, uv4lUrl = null, uv4lRestUrl = null) {
    this._serverUrl = serverUrl;
    this._roomId = roomId;
    this._passiveMode = passiveMode || false;
    this._debug = debug || false;


    // Internal
    this._socket = null;
    this._localStream = null;
    this._peerConnections = {};
    this._me = new User('me', name);

    // uv4l
    this._uv4l = uv4lUrl !== null;
    this._uv4lUrl = uv4lUrl;
    this._uv4lPeerConnections = {};

    // iOS safari image is rotated by 90
    this._isSafariIOS = isSafariIOS || false;
    this._uv4lRestUrl = uv4lRestUrl;
  }

  // Must be overriden
  onAddVideoStream(stream, user) { } // eslint-disable-line no-unused-vars, class-methods-use-this
  onRemoveVideoStream(userId) { } // eslint-disable-line no-unused-vars, class-methods-use-this

  // Can be overriden
  onError(error) { } // eslint-disable-line no-unused-vars, class-methods-use-this
  onUserSignup(user) { } // eslint-disable-line no-unused-vars, class-methods-use-this
  onUserDisconnected(userId) { } // eslint-disable-line no-unused-vars, class-methods-use-this

  debug(...messages) {
    if (this._debug) {
      console.log('[Connection]', ...messages);
    }
  }

  error(...messages) {
    if (this._debug) {
      console.error('[Connection]', ...messages);
    }
  }

  start() {
    this.debug('start');

    if (this._uv4l) {
      this._connectToServer();
    } else if (this._passiveMode) {
      this._connectToServer();
    } else {
      this._startCamera();
    }
  }

  stop() {
    this.debug('stop');

    if (this._socket != null) {
      this._socket.close();
      this._socket = null;
    }

    this._stopCamera();

    const self = this;

    Object.keys(this._uv4lPeerConnections).forEach((userId) => {
      self._uv4lRemovePeer(userId, 'hangup');
    });
  }

  // CAMERA

  _startCamera() {
    this.debug('startCamera');

    const self = this;
    this._compatGetUserMedia({ video: true, audio: true }, (stream) => {
      self._onCameraSuccess(stream);
    }, (error) => {
      self._onCameraError(error);
    });
  }

  _stopCamera() {
    this.debug('stopCamera');

    this.onRemoveVideoStream('me');

    if (this._localStream == null) {
      return;
    }

    // stop() on stream is deprecated
    this._localStream.getTracks().forEach((it) => {
      it.stop();
    });
    this._localStream = null;
  }

  _onCameraSuccess(stream) {
    this.debug('onCameraSuccess', stream);

    this._localStream = stream;
    this.onAddVideoStream(stream, this._me, true);

    this._connectToServer();
  }

  _onCameraError(error) {
    this.debug('onCameraError', error);

    this.onError(error);
  }

  _compatGetUserMedia(constraints, onStartVideoSuccess, onStartVideoError) {
    this.debug('getUserMedia', constraints);

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia(constraints)
        .then(onStartVideoSuccess)
        .catch(onStartVideoError);
    } else if (navigator.getUserMedia) {
      navigator.getUserMedia(constraints, onStartVideoSuccess, onStartVideoError);
    } else if (navigator.webkitGetUserMedia) {
      navigator.webkitGetUserMedia(constraints, onStartVideoSuccess, onStartVideoError);
    } else if (navigator.mozGetUserMedia) {
      navigator.mozGetUserMedia(constraints, onStartVideoSuccess, onStartVideoError);
    } else {
      this.error('getUserMedia not implemented');
      this.onError(Error('getUserMedia not implemented'));
    }
  }

  // SOCKET

  _connectToServer() {
    this.debug('connectToServer');

    this._socket = io(this._serverUrl);

    const self = this;
    this._socket.on('connect', () => {
      self._onConnect();
    });
  }

  _onConnect() {
    this.debug('onConnect');

    const self = this;

    this._socket.on('message', (message) => {
      self._processMessage(message);
    });

    const signupMessage = {
      roomId: this._roomId,
      passiveMode: this._passiveMode,
      name: atob(this._me.name),
      isSafariIOS: this._isSafariIOS,
    };

    this._sendMessage('signup', signupMessage);
  }

  _sendMessage(type, content, dstId) {
    this.debug('sendMessage', type, content, dstId);

    const message = {
      srcId: this._socket.id,
      type,
      content,
    };

    // By default send the message to everyone in the room
    if (dstId) {
      message.dstId = dstId;
    }

    this._socket.emit('message', message);
  }

  _processMessage(message) {
    this.debug('processMessage', message);

    if (message.type === 'user_signup') {
      const user = new User(message.srcId, btoa(message.content.name));
      const isSafariIOS = message.content.isSafariIOS;

      if (this._uv4l) {
        this._uv4lCreatePeer(user, true, isSafariIOS);
      } else if (this._passiveMode === false) {
        //  uv4l doesn't support to not be the initiator
        // this._createPeer(user, true);
        this._sendMessage('call', { name: atob(this._me.name), isSafariIOS: this._isSafariIOS }, message.srcId);
      }

      this.onUserSignup(user);
    } else if (message.type === 'signal') {
      if (!this._peerConnections[message.srcId] && !this._uv4lPeerConnections[message.srcId]) {
        const user = new User(message.srcId, btoa(message.content.name));

        if (this._uv4l) {
          // uv4l doesn't support to not be the initiator
          // this._uv4lCreatePeer(user, false);
          this.debug('ignore signal request receive on uv4l client');
          return;
        }
        // Not supposed to happen, passive user don't receive `signal`
        else if (this._passiveMode === false) {
          this._createPeer(user, false);
        }

        this.onUserSignup(user);
      }

      // Intercept signal error, mostly problem of connexion
      try {
        if (this._uv4lPeerConnections[message.srcId]) {
          if (message.content.signal.type === 'answer') {
            this._uv4lSend(message.srcId, {
              what: 'answer',
              data: message.content.signal,
            });
            this._uv4lSend(message.srcId, {
              what: 'generateIceCandidates',
            });
          } else if (message.content.signal.type === 'offer') {
            this._uv4lSend(message.srcId, {
              what: 'offer',
              data: message.content.signal,
            });
          } else if (message.content.signal.candidate != null) {
            this._uv4lSend(message.srcId, {
              what: 'addIceCandidate',
              data: message.content.signal.candidate,
            });
          } else {
            this.debug('[WebSocket]', 'unknown message for uv4l', message);
          }
        } else if (this._peerConnections[message.srcId]) {
          this._peerConnections[message.srcId].signal(message.content.signal);
        }
      } catch (error) {
        this.onError(error);
      }
    } else if (message.type === 'call') {
      if (this._uv4l) {
        const user = new User(message.srcId, btoa(message.content.name));
        const isSafariIOS = message.content.isSafariIOS;
        this._uv4lCreatePeer(user, true, isSafariIOS);
        this.onUserSignup(user);
      } else {
        this.error('call request receive on not uv4l client');
        this.onError(Error('call request receive on not uv4l client'));
      }
    } else if (message.type === 'user_disconnected') {
      if (this._passiveMode === false) {
        this._removePeer(message.srcId);
        this._uv4lRemovePeer(message.srcId);
        this.onRemoveVideoStream(message.srcId);
      }
      this.onUserDisconnected(message.srcId);
    } else if (message.type === 'error') {
      this.error('error message', message.content);
      this.onError(Error(message.content));
    } else {
      this.error('unknown message', message.type, message);
    }
  }

  // SIMPLE PEER

  _createPeer(user, initiator) {
    this.debug('createPeer', user, initiator);

    if (!SimplePeer.WEBRTC_SUPPORT) {
      this.error("doesn't support WebRTC");
      this.onError(Error("doesn't support WebRTC"));
      return;
    }

    const peer = new SimplePeer({
      initiator,
      stream: this._localStream,
      config: {
        iceServers: [
          { urls: 'stun:stun.l.google.com:19302' },
          { urls: 'stun:stun1.l.google.com:19302' },
          { urls: 'stun:stun2.l.google.com:19302' },
          { urls: 'stun:stun3.l.google.com:19302' },
          { urls: 'stun:stun4.l.google.com:19302' },
          { urls: 'stun:23.21.150.121' },
          { urls: 'stun:stun01.sipphone.com' },
          { urls: 'stun:stun.ekiga.net' },
          { urls: 'stun:stun.fwdnet.net' },
          { urls: 'stun:stun.ideasip.com' },
          {
            urls: 'turn:numb.viagenie.ca',
            credential: 'loup',
            username: 'louptester@gmail.com',
          },
          {
            urls: 'turn:192.158.29.39:3478?transport=udp',
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808',
          },
          {
            urls: 'turn:192.158.29.39:3478?transport=tcp',
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808',
          },
          {
            urls: 'turn:turn.bistri.com:80',
            credential: 'homeo',
            username: 'homeo',
          },
          {
            urls: 'turn:turn.anyfirewall.com:443?transport=tcp',
            credential: 'webrtc',
            username: 'webrtc',
          },
        ],
      },
    });
    this._peerConnections[user.id] = peer;

    const self = this;

    peer.on('signal', (data) => {
      const signalMessage = {
        signal: data,
        name: atob(self._me.name),
      };
      self._sendMessage('signal', signalMessage, user.id);
    });

    peer.on('stream', (stream) => {
      self.debug('onAddVideoStream', user, stream);
      self.onAddVideoStream(stream, user, false);
    });

    peer.on('error', (error) => {
      self.error('Peer error', error);
    });
  }

  _removePeer(userId) {
    this.debug('removePeer');

    delete this._peerConnections[userId];
  }

  // uv4l

  _uv4lCreatePeer(user, initiator, isSafariIOS) {
    this.debug('[WebSocket]', 'uv4lCreatePeer', user, initiator, isSafariIOS);

    if (this._uv4lPeerConnections[user.id]) {
      this.debug('[WebSocket] socket in creation');
      return;
    }

    const self = this;
    const url = `${this._uv4lRestUrl}/api/webrtc/settings`;

    this.debug('uv4l get settings');
    axios.get(url).then(function (response) {
      self.debug('uv4l get settings - done');
      self.debug(response.data);

      const newData = response.data;
      newData.output.rotation = isSafariIOS ? 90 : 0;

      self.debug('uv4l update settings');
      self.debug(newData);

      self.debug('uv4l put settings');
      axios({
        method: 'put',
        url,
        data: newData,
      }).then(function (response) {
        self.debug('uv4l put settings - done');
        self.debug(response.data);

        self._uv4lSetupPeer(user, initiator);
      });
    });
  }

  _uv4lSetupPeer(user, initiator) {
    const peer = new WebSocket(this._uv4lUrl);
    this._uv4lPeerConnections[user.id] = peer;
    const self = this;

    peer.onopen = function () {
      self.debug('[WebSocket]', 'opened');

      if (initiator) {
        self._uv4lSend(user.id, {
          what: 'call',
          options: {
            force_hw_vcodec: true,
            vformat: 30,
          },
        });
      }
    };

    peer.onclose = function (event) {
      self.debug('[WebSocket]', 'closed', event);
    };

    peer.onerror = function (error) {
      self.error('[WebSocket]', 'error', error);
      self.onError(Error('[WebSocket]', 'error', error));
    };

    peer.onmessage = function (e) {
      const data = JSON.parse(e.data);

      self.debug('[WebSocket]', 'message', data);

      if (data.what === 'message') {
        self.debug('[WebSocket]', 'error', data.message);
        self.onError(Error(data.message));
      } else if (data.what === 'offer') {
        const signalMessage = {
          signal: JSON.parse(data.data),
          name: atob(self._me.name),
        };
        self._sendMessage('signal', signalMessage, user.id);
      } else if (data.what === 'answer') {
        const signalMessage = {
          signal: JSON.parse(data.data),
          name: atob(self._me.name),
        };
        self._sendMessage('signal', signalMessage, user.id);
      } else if (data.what === 'iceCandidates') {
        const iceCandidates = JSON.parse(data.data);
        iceCandidates.forEach((iceCandidate) => {
          const signalMessage = {
            signal: { candidate: iceCandidate },
            name: atob(self._me.name),
          };
          self._sendMessage('signal', signalMessage, user.id);
        });
      } else {
        self.error('[WebSocket]', 'unknown message', e.data.data);
      }
    };
  }

  _uv4lSend(userId, message, retyCount = 0) {
    this.debug('[WebSocket]', 'uv4lSend', userId, message);

    const socket = this._uv4lPeerConnections[userId];

    if (!socket) {
      return;
    } else if (socket.readyState === 3) {
      this.debug('[WebSocket]', 'uv4lSend', 'socket closed');
      delete this._uv4lPeerConnections[userId];
      return;
    } else if (socket.readyState !== 1) {
      this.debug('[WebSocket]', 'socket not ready', socket.readyState, retyCount);

      const self = this;
      setTimeout(() => {
        self._uv4lSend(userId, message, retyCount + 1);
      }, 1000);

      return;
    }

    if (message.data) {
      message.data = JSON.stringify(message.data);
    }

    socket.send(JSON.stringify(message));
  }

  _uv4lRemovePeer(userId) {
    this.debug('[WebSocket]', 'uv4lRemovePeer', userId);

    this._uv4lSend(userId, {
      what: 'hangup',
    });

    const socket = this._uv4lPeerConnections[userId];
    if (socket) {
      socket.close();
    }

    delete this._uv4lPeerConnections[userId];
  }

}

module.exports = Connection;
