'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* global navigator */

var SimplePeer = require('simple-peer');
var io = require('socket.io-client');
var axios = require('axios');

// encode base 64
function atob(text) {
  return new Buffer(text).toString('base64');
}

// decode base 64
function btoa(base64) {
  return new Buffer(base64, 'base64').toString();
}

var User = function User(id, name) {
  _classCallCheck(this, User);

  this.id = id;
  this.name = name;
};

var Connection = function () {
  function Connection(name, serverUrl, roomId, passiveMode, debug) {
    var isSafariIOS = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
    var uv4lUrl = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : null;
    var uv4lRestUrl = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : null;

    _classCallCheck(this, Connection);

    this._serverUrl = serverUrl;
    this._roomId = roomId;
    this._passiveMode = passiveMode || false;
    this._debug = debug || false;

    // Internal
    this._socket = null;
    this._localStream = null;
    this._peerConnections = {};
    this._me = new User('me', name);

    // uv4l
    this._uv4l = uv4lUrl !== null;
    this._uv4lUrl = uv4lUrl;
    this._uv4lPeerConnections = {};

    // iOS safari image is rotated by 90
    this._isSafariIOS = isSafariIOS || false;
    this._uv4lRestUrl = uv4lRestUrl;
  }

  // Must be overriden


  _createClass(Connection, [{
    key: 'onAddVideoStream',
    value: function onAddVideoStream(stream, user) {} // eslint-disable-line no-unused-vars, class-methods-use-this

  }, {
    key: 'onRemoveVideoStream',
    value: function onRemoveVideoStream(userId) {} // eslint-disable-line no-unused-vars, class-methods-use-this

    // Can be overriden

  }, {
    key: 'onError',
    value: function onError(error) {} // eslint-disable-line no-unused-vars, class-methods-use-this

  }, {
    key: 'onUserSignup',
    value: function onUserSignup(user) {} // eslint-disable-line no-unused-vars, class-methods-use-this

  }, {
    key: 'onUserDisconnected',
    value: function onUserDisconnected(userId) {} // eslint-disable-line no-unused-vars, class-methods-use-this

  }, {
    key: 'debug',
    value: function debug() {
      if (this._debug) {
        var _console;

        for (var _len = arguments.length, messages = Array(_len), _key = 0; _key < _len; _key++) {
          messages[_key] = arguments[_key];
        }

        (_console = console).log.apply(_console, ['[Connection]'].concat(messages));
      }
    }
  }, {
    key: 'error',
    value: function error() {
      if (this._debug) {
        var _console2;

        for (var _len2 = arguments.length, messages = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          messages[_key2] = arguments[_key2];
        }

        (_console2 = console).error.apply(_console2, ['[Connection]'].concat(messages));
      }
    }
  }, {
    key: 'start',
    value: function start() {
      this.debug('start');

      if (this._uv4l) {
        this._connectToServer();
      } else if (this._passiveMode) {
        this._connectToServer();
      } else {
        this._startCamera();
      }
    }
  }, {
    key: 'stop',
    value: function stop() {
      this.debug('stop');

      if (this._socket != null) {
        this._socket.close();
        this._socket = null;
      }

      this._stopCamera();

      var self = this;

      Object.keys(this._uv4lPeerConnections).forEach(function (userId) {
        self._uv4lRemovePeer(userId, 'hangup');
      });
    }

    // CAMERA

  }, {
    key: '_startCamera',
    value: function _startCamera() {
      this.debug('startCamera');

      var self = this;
      this._compatGetUserMedia({ video: true, audio: true }, function (stream) {
        self._onCameraSuccess(stream);
      }, function (error) {
        self._onCameraError(error);
      });
    }
  }, {
    key: '_stopCamera',
    value: function _stopCamera() {
      this.debug('stopCamera');

      this.onRemoveVideoStream('me');

      if (this._localStream == null) {
        return;
      }

      // stop() on stream is deprecated
      this._localStream.getTracks().forEach(function (it) {
        it.stop();
      });
      this._localStream = null;
    }
  }, {
    key: '_onCameraSuccess',
    value: function _onCameraSuccess(stream) {
      this.debug('onCameraSuccess', stream);

      this._localStream = stream;
      this.onAddVideoStream(stream, this._me, true);

      this._connectToServer();
    }
  }, {
    key: '_onCameraError',
    value: function _onCameraError(error) {
      this.debug('onCameraError', error);

      this.onError(error);
    }
  }, {
    key: '_compatGetUserMedia',
    value: function _compatGetUserMedia(constraints, onStartVideoSuccess, onStartVideoError) {
      this.debug('getUserMedia', constraints);

      if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).then(onStartVideoSuccess).catch(onStartVideoError);
      } else if (navigator.getUserMedia) {
        navigator.getUserMedia(constraints, onStartVideoSuccess, onStartVideoError);
      } else if (navigator.webkitGetUserMedia) {
        navigator.webkitGetUserMedia(constraints, onStartVideoSuccess, onStartVideoError);
      } else if (navigator.mozGetUserMedia) {
        navigator.mozGetUserMedia(constraints, onStartVideoSuccess, onStartVideoError);
      } else {
        this.error('getUserMedia not implemented');
        this.onError(Error('getUserMedia not implemented'));
      }
    }

    // SOCKET

  }, {
    key: '_connectToServer',
    value: function _connectToServer() {
      this.debug('connectToServer');

      this._socket = io(this._serverUrl);

      var self = this;
      this._socket.on('connect', function () {
        self._onConnect();
      });
    }
  }, {
    key: '_onConnect',
    value: function _onConnect() {
      this.debug('onConnect');

      var self = this;

      this._socket.on('message', function (message) {
        self._processMessage(message);
      });

      var signupMessage = {
        roomId: this._roomId,
        passiveMode: this._passiveMode,
        name: atob(this._me.name),
        isSafariIOS: this._isSafariIOS
      };

      this._sendMessage('signup', signupMessage);
    }
  }, {
    key: '_sendMessage',
    value: function _sendMessage(type, content, dstId) {
      this.debug('sendMessage', type, content, dstId);

      var message = {
        srcId: this._socket.id,
        type: type,
        content: content
      };

      // By default send the message to everyone in the room
      if (dstId) {
        message.dstId = dstId;
      }

      this._socket.emit('message', message);
    }
  }, {
    key: '_processMessage',
    value: function _processMessage(message) {
      this.debug('processMessage', message);

      if (message.type === 'user_signup') {
        var user = new User(message.srcId, btoa(message.content.name));
        var isSafariIOS = message.content.isSafariIOS;

        if (this._uv4l) {
          this._uv4lCreatePeer(user, true, isSafariIOS);
        } else if (this._passiveMode === false) {
          //  uv4l doesn't support to not be the initiator
          // this._createPeer(user, true);
          this._sendMessage('call', { name: atob(this._me.name), isSafariIOS: this._isSafariIOS }, message.srcId);
        }

        this.onUserSignup(user);
      } else if (message.type === 'signal') {
        if (!this._peerConnections[message.srcId] && !this._uv4lPeerConnections[message.srcId]) {
          var _user = new User(message.srcId, btoa(message.content.name));

          if (this._uv4l) {
            // uv4l doesn't support to not be the initiator
            // this._uv4lCreatePeer(user, false);
            this.debug('ignore signal request receive on uv4l client');
            return;
          }
          // Not supposed to happen, passive user don't receive `signal`
          else if (this._passiveMode === false) {
              this._createPeer(_user, false);
            }

          this.onUserSignup(_user);
        }

        // Intercept signal error, mostly problem of connexion
        try {
          if (this._uv4lPeerConnections[message.srcId]) {
            if (message.content.signal.type === 'answer') {
              this._uv4lSend(message.srcId, {
                what: 'answer',
                data: message.content.signal
              });
              this._uv4lSend(message.srcId, {
                what: 'generateIceCandidates'
              });
            } else if (message.content.signal.type === 'offer') {
              this._uv4lSend(message.srcId, {
                what: 'offer',
                data: message.content.signal
              });
            } else if (message.content.signal.candidate != null) {
              this._uv4lSend(message.srcId, {
                what: 'addIceCandidate',
                data: message.content.signal.candidate
              });
            } else {
              this.debug('[WebSocket]', 'unknown message for uv4l', message);
            }
          } else if (this._peerConnections[message.srcId]) {
            this._peerConnections[message.srcId].signal(message.content.signal);
          }
        } catch (error) {
          this.onError(error);
        }
      } else if (message.type === 'call') {
        if (this._uv4l) {
          var _user2 = new User(message.srcId, btoa(message.content.name));
          var _isSafariIOS = message.content.isSafariIOS;
          this._uv4lCreatePeer(_user2, true, _isSafariIOS);
          this.onUserSignup(_user2);
        } else {
          this.error('call request receive on not uv4l client');
          this.onError(Error('call request receive on not uv4l client'));
        }
      } else if (message.type === 'user_disconnected') {
        if (this._passiveMode === false) {
          this._removePeer(message.srcId);
          this._uv4lRemovePeer(message.srcId);
          this.onRemoveVideoStream(message.srcId);
        }
        this.onUserDisconnected(message.srcId);
      } else if (message.type === 'error') {
        this.error('error message', message.content);
        this.onError(Error(message.content));
      } else {
        this.error('unknown message', message.type, message);
      }
    }

    // SIMPLE PEER

  }, {
    key: '_createPeer',
    value: function _createPeer(user, initiator) {
      this.debug('createPeer', user, initiator);

      if (!SimplePeer.WEBRTC_SUPPORT) {
        this.error("doesn't support WebRTC");
        this.onError(Error("doesn't support WebRTC"));
        return;
      }

      var peer = new SimplePeer({
        initiator: initiator,
        stream: this._localStream,
        config: {
          iceServers: [{ urls: 'stun:stun.l.google.com:19302' }, { urls: 'stun:stun1.l.google.com:19302' }, { urls: 'stun:stun2.l.google.com:19302' }, { urls: 'stun:stun3.l.google.com:19302' }, { urls: 'stun:stun4.l.google.com:19302' }, { urls: 'stun:23.21.150.121' }, { urls: 'stun:stun01.sipphone.com' }, { urls: 'stun:stun.ekiga.net' }, { urls: 'stun:stun.fwdnet.net' }, { urls: 'stun:stun.ideasip.com' }, {
            urls: 'turn:numb.viagenie.ca',
            credential: 'loup',
            username: 'louptester@gmail.com'
          }, {
            urls: 'turn:192.158.29.39:3478?transport=udp',
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808'
          }, {
            urls: 'turn:192.158.29.39:3478?transport=tcp',
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808'
          }, {
            urls: 'turn:turn.bistri.com:80',
            credential: 'homeo',
            username: 'homeo'
          }, {
            urls: 'turn:turn.anyfirewall.com:443?transport=tcp',
            credential: 'webrtc',
            username: 'webrtc'
          }]
        }
      });
      this._peerConnections[user.id] = peer;

      var self = this;

      peer.on('signal', function (data) {
        var signalMessage = {
          signal: data,
          name: atob(self._me.name)
        };
        self._sendMessage('signal', signalMessage, user.id);
      });

      peer.on('stream', function (stream) {
        self.debug('onAddVideoStream', user, stream);
        self.onAddVideoStream(stream, user, false);
      });

      peer.on('error', function (error) {
        self.error('Peer error', error);
      });
    }
  }, {
    key: '_removePeer',
    value: function _removePeer(userId) {
      this.debug('removePeer');

      delete this._peerConnections[userId];
    }

    // uv4l

  }, {
    key: '_uv4lCreatePeer',
    value: function _uv4lCreatePeer(user, initiator, isSafariIOS) {
      this.debug('[WebSocket]', 'uv4lCreatePeer', user, initiator, isSafariIOS);

      if (this._uv4lPeerConnections[user.id]) {
        this.debug('[WebSocket] socket in creation');
        return;
      }

      var self = this;
      var url = this._uv4lRestUrl + '/api/webrtc/settings';

      this.debug('uv4l get settings');
      axios.get(url).then(function (response) {
        self.debug('uv4l get settings - done');
        self.debug(response.data);

        var newData = response.data;
        newData.output.rotation = isSafariIOS ? 90 : 0;

        self.debug('uv4l update settings');
        self.debug(newData);

        self.debug('uv4l put settings');
        axios({
          method: 'put',
          url: url,
          data: newData
        }).then(function (response) {
          self.debug('uv4l put settings - done');
          self.debug(response.data);

          self._uv4lSetupPeer(user, initiator);
        });
      });
    }
  }, {
    key: '_uv4lSetupPeer',
    value: function _uv4lSetupPeer(user, initiator) {
      var peer = new WebSocket(this._uv4lUrl);
      this._uv4lPeerConnections[user.id] = peer;
      var self = this;

      peer.onopen = function () {
        self.debug('[WebSocket]', 'opened');

        if (initiator) {
          self._uv4lSend(user.id, {
            what: 'call',
            options: {
              force_hw_vcodec: true,
              vformat: 30
            }
          });
        }
      };

      peer.onclose = function (event) {
        self.debug('[WebSocket]', 'closed', event);
      };

      peer.onerror = function (error) {
        self.error('[WebSocket]', 'error', error);
        self.onError(Error('[WebSocket]', 'error', error));
      };

      peer.onmessage = function (e) {
        var data = JSON.parse(e.data);

        self.debug('[WebSocket]', 'message', data);

        if (data.what === 'message') {
          self.debug('[WebSocket]', 'error', data.message);
          self.onError(Error(data.message));
        } else if (data.what === 'offer') {
          var signalMessage = {
            signal: JSON.parse(data.data),
            name: atob(self._me.name)
          };
          self._sendMessage('signal', signalMessage, user.id);
        } else if (data.what === 'answer') {
          var _signalMessage = {
            signal: JSON.parse(data.data),
            name: atob(self._me.name)
          };
          self._sendMessage('signal', _signalMessage, user.id);
        } else if (data.what === 'iceCandidates') {
          var iceCandidates = JSON.parse(data.data);
          iceCandidates.forEach(function (iceCandidate) {
            var signalMessage = {
              signal: { candidate: iceCandidate },
              name: atob(self._me.name)
            };
            self._sendMessage('signal', signalMessage, user.id);
          });
        } else {
          self.error('[WebSocket]', 'unknown message', e.data.data);
        }
      };
    }
  }, {
    key: '_uv4lSend',
    value: function _uv4lSend(userId, message) {
      var retyCount = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

      this.debug('[WebSocket]', 'uv4lSend', userId, message);

      var socket = this._uv4lPeerConnections[userId];

      if (!socket) {
        return;
      } else if (socket.readyState === 3) {
        this.debug('[WebSocket]', 'uv4lSend', 'socket closed');
        delete this._uv4lPeerConnections[userId];
        return;
      } else if (socket.readyState !== 1) {
        this.debug('[WebSocket]', 'socket not ready', socket.readyState, retyCount);

        var self = this;
        setTimeout(function () {
          self._uv4lSend(userId, message, retyCount + 1);
        }, 1000);

        return;
      }

      if (message.data) {
        message.data = JSON.stringify(message.data);
      }

      socket.send(JSON.stringify(message));
    }
  }, {
    key: '_uv4lRemovePeer',
    value: function _uv4lRemovePeer(userId) {
      this.debug('[WebSocket]', 'uv4lRemovePeer', userId);

      this._uv4lSend(userId, {
        what: 'hangup'
      });

      var socket = this._uv4lPeerConnections[userId];
      if (socket) {
        socket.close();
      }

      delete this._uv4lPeerConnections[userId];
    }
  }]);

  return Connection;
}();

module.exports = Connection;
